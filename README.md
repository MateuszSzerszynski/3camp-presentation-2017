# Prezentacja na Tech3camp w Gdańsku, Lipiec 2017

Temat: CSS Grid Layout, czyli jak przestałem się martwić i pokochałem budowanie interfejsu

## Jak zacząć

```
npm install
npm run start
```

- Strona z prezentacją będzie dostępna pod adresem `http://localhost:8080/presentation/#/`
- Pierwszy przykład - użycie CSS Grid Layout `http://localhost:8080/grid/`
- Drugi przykład - użycie CSS Grid Layout i CSS Variables `http://localhost:8080/grid-responsive/`


## Czego prezentacja używa
* Silnik na którym powstała prezentacja to Reveal.js
* Do prezentacji użyto także `font-awesome`